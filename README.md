# 5V/1A POWER-SUPPLY

The purpose of this page is to explain step by step the realization of an ultra low noise 5V power supply.

It has been designed to power a Topping E30 DAC.

The power supply uses the following components :

 * an 8V 10VA trransformer
 * 4 MUR420 diodes
 * 6 2200µF 25V capacitors
 * a 100nF and a 100pF capacitors
 * a LT3045 regulator module

### ELECTRONICS

The schema is made using KICAD.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2021/01/alimentation-5v-faible-bruit.html

